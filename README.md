# Docker lftp

Dockerhub link: https://hub.docker.com/r/tothlp/lftp

# About this repository
This repository hosts the dockerfile for an Ubuntu-based image (Ubuntu 20.04), with the latest available lftp package from the Official Ubuntu repositories.

# Usage
Usage is pretty much the same as any for any Ubuntu image.